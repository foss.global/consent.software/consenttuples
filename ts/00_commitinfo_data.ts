/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@consentsoftware/consenttuples',
  version: '2.0.1',
  description: 'npm package combining explanations and consent levels for several known third party analytics and tracking providers'
}
