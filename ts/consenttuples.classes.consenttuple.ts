import * as plugins from './consenttuples.plugins.js';

export class ConsentTuple implements plugins.csInterfaces.IConsentTuple {
  public static getGoogleAnalytics4Tuple(googleAnalytics4Id?: string) {
    const googleAnalytics4Tuple = new ConsentTuple({
      name: 'Google Analytics 4',
      description: 'Google Analytics 4 is the current and most modern Google Analytics version',
      level: 'functional',
      script: async (ga4Code: string, cookieLevelsArg: plugins.csInterfaces.TCookieLevel[]) => {
        globalThis.dataLayer = globalThis.dataLayer || [];
        globalThis.gtag = function () {
          globalThis.dataLayer.push(arguments);
        };
        if (!cookieLevelsArg.includes('analytics') || !cookieLevelsArg.includes('all')) {
          console.log('GA: analytics and ad storage denied! Running in consent beta mode.');
          globalThis.gtag('consent', 'default', {
            ad_storage: 'denied',
            analytics_storage: 'denied',
          });
        }
        globalThis.gtag('js', new Date());
        globalThis.gtag('config', ga4Code);

        // lets load the gtag script
        const scriptTag = document.createElement('script');
        scriptTag.src = `https://www.googletagmanager.com/gtag/js?id=${ga4Code}`;
        scriptTag.async = true;
        document.head.append(scriptTag);
        console.log('GA: tag appended!');
      },
      scriptExecutionDataArg: googleAnalytics4Id,
    });
    return googleAnalytics4Tuple;
  }

  public static getGoogleAnalyticsUniversalTuple(googleAnaylticsUniversalIdArg?: string) {
    const googleAnalyticsUniversalTuple = new ConsentTuple({
      name: 'Google Analytics Universal',
      description: 'Google Analytics Universal is a legacy Analytics service from Google Inc',
      level: 'analytics',
      script: async (gaCode: string, cookieLevelsArg: plugins.csInterfaces.TCookieLevel[]) => {
        // tslint:disable-next-line: only-arrow-functions
        (function (i, s, o, g, r, a, m) {
          // tslint:disable-next-line: no-string-literal
          i['GoogleAnalyticsObject'] = r;
          // tslint:disable-next-line: ban-comma-operator
          (i[r] =
            i[r] ||
            // tslint:disable-next-line: only-arrow-functions
            function () {
              (i[r].q = i[r].q || []).push(arguments);
            }),
            (i[r].l = new Date().getTime());
          // tslint:disable-next-line: ban-comma-operator
          (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
          a.async = 1;
          a.src = g;
          a.crossorigin = 'anonymous';
          m.parentNode.insertBefore(a, m);
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'analytics');

        globalThis.analytics('create', gaCode, 'auto');
        globalThis.analytics('send', 'pageview');
        console.log(
          'Loaded Google Analytics. You may view our privacy policy at https://lossless.gmbh'
        );
      },
      scriptExecutionDataArg: googleAnaylticsUniversalIdArg,
    });
    return googleAnalyticsUniversalTuple;
  }

  name: string;
  description: string;
  level: plugins.csInterfaces.TCookieLevel;
  script: string | ((...args: any) => Promise<void>);
  scriptExecutionDataArg: any;

  constructor(optionsArg: plugins.csInterfaces.IConsentTuple) {
    this.name = optionsArg.name;
    this.description = optionsArg.description;
    this.level = optionsArg.level;
    this.script =
      typeof optionsArg.script === 'function'
        ? optionsArg.script
        : // tslint:disable-next-line: function-constructor
          (new Function(optionsArg.script) as any);
    this.scriptExecutionDataArg = optionsArg.scriptExecutionDataArg;
  }

  public getTransferableObject(): plugins.csInterfaces.IConsentTuple {
    return {
      name: this.name,
      description: this.description,
      level: this.level,
      script: typeof this.script === 'function' ? this.script.toString() : this.script,
      scriptExecutionDataArg: this.scriptExecutionDataArg,
    };
  }
}
